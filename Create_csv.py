import pandas as pd 
from pandas import DataFrame


Data={'Titles':["Cậu cả Brooklyn ngỗ nghịch nhà David Beckham: Bất tài nhiều tật, 5 năm 20 mối tình, từ o bế thành bị bố mẹ quay lưng",
        "Lý Tiểu Lộ dẫn con gái 7 tuổi đi chơi nhưng ai cũng phải bất ngờ với chiều cao và vóc dáng thiếu nữ của Điềm Hinh",
        "NÓNG: MBC tung bằng chứng bố Yang tổ chức sex tour trá hình từ châu Âu đến Hàn cho đại gia Malaysia và 10 gái mại dâm",
        "Dàn sao siêu phẩm 'Kí sinh trùng: Ảnh đế không hot bằng nữ hoàng 18+ và cặp bạn tài tử thị phi, sao nhí lột xác bất ngờ",
        "Mặc đơn giản, make-up sương sương dự sự kiện quốc tế, Lisa (BLACKPINK) gây choáng với nhan sắc thách thức mọi camera"],
        
        'Content':["Brooklyn sinh ngày 14/3/1999, là con trai đầu lòng của cặp đôi nổi tiếng và tai tiếng nhất thế giới lúc bấy giờ - danh thủ David Beckham và thành viên nhóm nhạc nữ Spice Girls - Victoria Beckham. Ban đầu, vì là con trai nên Brooklyn được kỳ vọng sẽ nối nghiệp thể thao của bố, trở thành tài năng bóng đá trẻ của nước Anh. Ngay từ khi còn bé tí, cậu bé Brooklyn đã chập chững theo bố đến sân cỏ để làm quen sớm. Hình ảnh Brooklyn 2 tuổi ôm trái bóng tròn to bằng cả thân người chạy từng bước chậm chạp trên sân cỏ Old Trafford đã trở thành cơn sốt trên các trang báo những năm đầu 2000. Ai cũng nghĩ rằng Brooklyn sau này hẳn sẽ là một tương lai đầy hứa hẹn của bóng đá Anh.",
        "Cuộc hôn nhân của Lý Tiểu Lộ và Giả Nãi Lượng coi như đã đi đến dấu chấm hết khi cả hai không còn bất cứ những hành động làm dấy lên nghi vấn vẫn còn ở bên nhau. Giờ đây, công chúng đã quá quen với hình ảnh Điềm Hinh - cô con gái nhỏ của 2 người hoặc đi chơi với bố, hoặc là ở bên mẹ. Năm ngoái, khi scandal ngoại tình nổ ra, công chúng đều lo cho tinh thần và cuộc sống của Điềm Hinh bị ảnh hưởng bởi cô bé còn quá nhỏ.Mới đây, Lý Tiểu Lộ chia sẻ hình ảnh đưa con gái đi chơi cùng với người bạn thân Đổng Tuyền và bé Lúm Đồng Tiền. Công chúng đều đặc biệt chú ý tới Điềm Hinh bởi chiều cao vượt trội cùng vóc dáng thiếu nữ khi bé chỉ mới 7 tuổi. Mặc chiếc váy đôi với mẹ, Điềm Hinh được dự đoán sẽ trở thành mỹ nhân tương lai giống như Tiểu Lộ.",
        'Đến hẹn lại lên, sau khi điều tra hàng loạt tình tiết của vụ bê bối Seungri, chương trình "Straight" của đài MBC lần này đã ra mặt để tung loạt bằng chứng môi giới mại dâm của cựu chủ tịch YG - Yang Hyun Suk. Theo đó, MBC tố bố Yang tổ chức một tour tiếp đã từ du lịch châu Âu cho đến tiệc thác loạn tại Hàn cho nhà đầu tư giàu có Malaysia và 10 gái mại dâm như một sex tour trá hình.',
        'Trong những ngày vừa qua, "Kí sinh trùng" trở thành siêu phẩm gây bão trên khắp ngõ ngách Hàn Quốc cho đến Việt Nam. Được biết, đây là một trong những tác phẩm điện ảnh gây tiếng vang lớn ở phạm vi thế giới của điện ảnh Hàn Quốc trong năm nay. Bên cạnh kịch bản độc đáo, dàn diễn viên bao gồm sao gạo cội và sao trẻ 9X cũng góp phần làm nên độ hot, thành công của bộ phim. Dàn sao "Kí sinh trùng" hầu như đều là những gương mặt đã nổi tiếng tại xứ Hàn như "nữ hoàng phim nóng", tài tử hạng A, sao nhí đình đám nhưng vẫn còn chưa được khán giả Việt quá quan tâm, ngoại trừ nam phụ nổi nhất phim Park Seo Joon.',
        'Hôm qua Lisa được bắt gặp đang dạo phố tại Paris thôi mà những hình ảnh từ nhiếp ảnh gia, phóng viên chụp lại đã gây bão mạng xã hội vì trông chẳng khác gì chụp lookbook hay bìa tạp chí. Thế rồi sang đến ngày hôm nay (24/6), khi xuất hiện tại show diễn của Celine với tư cách khách mời, dù chỉ mặc đồ hết sức đơn giản và make-up chuẩn sương sương cơ mà Lisa đã lại một lần nữa chứng minh nhan sắc thách thức mọi thể loại camera của mình.'],
        "Abstract":["aaaaa","bbbbb","cccccc","ddddd","eeeeeee"],
        "Category":["giai-tri","kinh-te","du-lich","giai-tri","cong-nghe"],
      
      "scores":[0.555,0.6789,0.69999,0.7122,0.88888]
}

df = DataFrame(Data,columns= ['Titles', 'Content','Abstract','Category','scores'])

df.to_csv("/home/tuananh/Documents/label_category/Data.csv")

