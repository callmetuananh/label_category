from  PyQt5.uic import loadUi
import sys
import pandas as pd
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QAction, QLineEdit, QMessageBox
from PyQt5.QtWidgets import QDialog, QApplication, QFileDialog,QMessageBox


from PyQt5.QtCore import Qt 
class set(QDialog):
    def __init__(self):
        super(set,self).__init__()
        loadUi("untitled.ui",self)
        self.btn_load.clicked.connect(self.load_click)
        self.takeprocess.setValue(0)        

        
    
    def load_click(self):
        #data=pd.read_csv("")
        self.path=QFileDialog.getOpenFileName()[0]
        if self.path.split(".")[-1]!="csv":
            QMessageBox.question(self,'PyQt5 message',"Vui lòng nhập đúng file csv", QMessageBox.Ok, QMessageBox.Ok)

        self.txt_path.setText(self.path)
        self.txt_path.setReadOnly(True)
        self.Data=pd.read_csv(self.path)
        
        self.movebutton.clicked.connect(self.move_click)

        self.len_data=self.Data.shape[0]
        status=True
        self.index_file=0
        self.showData()

        
            
        
    def move_click(self):
        try:  
            self.index_file=int(self.textEdit_4.toPlainText())
         
            
            if self.index_file >=0 and self.index_file <=self.len_data:
                self.showData()
        except:
            pass    

    def showData(self):
        self.Data_Index=self.Data.iloc[self.index_file]
        
        self.index.setText("Index :"+str(self.index_file))
        self.textEdit.setText(str(self.Data_Index["probability"]))
        self.textEdit.setReadOnly(True)
        self.textEdit_2.setText(self.Data_Index["full_content"])
        self.textEdit_2.setReadOnly(True)
        self.textEdit_3.setText(self.Data_Index["tag_title_abstract"])
        self.textEdit_3.setReadOnly(True)
        self.textCate.setText(self.Data_Index["Category"])
        self.textCate.setReadOnly(True)
        self.catee=self.Data_Index["Category"]


        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)


        #list_btn_Cat=[self.btn_congnghe,self.btn_doisong,self.btn_dulich,self.btn_giaitri,self.btn_giaoduc,self.btn_khoahoc,self.btn_kinhdoanh,self.btn_phapluat,self.btn_suckhoe,self.btn_thegioi,self.btn_thethao,self.btn_thoisu,self.btn_xe]
        
        self.btn_congnghe.clicked.connect(self.congnghe)
        self.btn_doisong.clicked.connect(self.doisong)
        self.btn_dulich.clicked.connect(self.dulich)
        self.btn_giaitri.clicked.connect(self.giaitri)
        self.btn_giaoduc.clicked.connect(self.giaoduc)
        self.btn_khoahoc.clicked.connect(self.khoahoc)
        self.btn_kinhdoanh.clicked.connect(self.kinhdoanh)
        self.btn_phapluat.clicked.connect(self.phapluat)
        self.btn_suckhoe.clicked.connect(self.suckhoe)
        self.btn_thegioi.clicked.connect(self.thegioi)
        self.btn_thethao.clicked.connect(self.thethao)
        self.btn_thoisu.clicked.connect(self.thoisu)
        self.btn_xe.clicked.connect(self.xe)


        self.btns_congnghe.clicked.connect(self.congnghes)
        self.btns_doisong.clicked.connect(self.doisongs)
        self.btns_dulich.clicked.connect(self.dulichs)
        self.btns_giaitri.clicked.connect(self.giaitris)
        self.btns_giaoduc.clicked.connect(self.giaoducs)
        self.btns_khoahoc.clicked.connect(self.khoahocs)
        self.btns_kinhdoanh.clicked.connect(self.kinhdoanhs)
        self.btns_phapluat.clicked.connect(self.phapluats)
        self.btns_suckhoe.clicked.connect(self.suckhoes)
        self.btns_thegioi.clicked.connect(self.thegiois)
        self.btns_thethao.clicked.connect(self.thethaos)
        self.btns_thoisu.clicked.connect(self.thoisus)
        self.btns_xe.clicked.connect(self.xes)

       
            

            

    # *****************************************************************
    # Cat chinh


            
        
    def congnghe(self):
        # self.cat_current="cong-nghe"
        # self.Data=self.Data.set_value(self.index_file,"main_cat","dcmmm")
        
        # self.txt_main.setText(self.Data_Index["main_cat"])

        # self.textCate.setReadOnly(True)
        self.cat_current="cong-nghe"

        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)
       
        
        
    def doisong(self):
        
        self.cat_current="doi-song"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

        
    
    def dulich(self):
       
        self.cat_current="du-lich"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    
    def giaitri(self):
        
        self.cat_current="giai-tri"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)
    
    def giaoduc(self):
       
        self.cat_current="giao-duc"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)


    def khoahoc(self):
        
        self.cat_current="khoa-hoc"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def kinhdoanh(self):
        
        
        self.cat_current="kinh-doanh"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def phapluat(self):
       
        
        self.cat_current="phap-luat"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def suckhoe(self):
        
        self.cat_current="suc-khoe"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def thegioi(self):
        self.cat_current="the-gioi"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def thethao(self):
        
        self.cat_current="the-thao"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    def thoisu(self):
        
        self.cat_current="thoi-su"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)


    def xe(self):
        
        self.cat_current="xe"
        self.Data_Index["main_cat"]=self.Data_Index["main_cat"].replace(self.Data_Index["main_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_main.setText(self.Data_Index["main_cat"])
        self.txt_main.setReadOnly(True)

    #===========================================================================================
    # SUB CAT
    def congnghes(self):
        
        self.cat_current="cong-nghe"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)
       
        
        
    def doisongs(self):
        
        self.cat_current="doi-song"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

        
    
    def dulichs(self):
       
        self.cat_current="du-lich"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)
    
    def giaitris(self):
        
        self.cat_current="giai-tri"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)
    
    def giaoducs(self):
       
        self.cat_current="giao-duc"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def khoahocs(self):
        
        self.cat_current="khoa-hoc"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def kinhdoanhs(self):
        
        
        self.cat_current="kinh-doanh"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)
    
    def phapluats(self):
       
        
        self.cat_current="phap-luat"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def suckhoes(self):
        
        self.cat_current="suc-khoe"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def thegiois(self):
        self.cat_current="the-gioi"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def thethaos(self):
        
        self.cat_current="the-thao"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def thoisus(self):
        
        self.cat_current="thoi-su"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    def xes(self):
        
        self.cat_current="xe"
        self.Data_Index["sub_cat"]=self.Data_Index["sub_cat"].replace(self.Data_Index["sub_cat"],self.cat_current)
        self.Data.iloc[self.index_file]=self.Data_Index
        self.txt_sub.setText(self.Data_Index["sub_cat"])
        self.txt_sub.setReadOnly(True)

    
    def keyPressEvent(self, e):
        
        if e.key() == Qt.Key_D:
            self.takeprocess.setValue(0)
            

            if self.index_file<self.len_data-1:
                
                self.index_file=self.index_file+1
                self.showData()
            


        elif e.key() == Qt.Key_A:
            
            self.takeprocess.setValue(0)
            if self.index_file>0:
                self.index_file=self.index_file-1
                self.showData()
        
        elif e.key()==Qt.Key_S:
            self.count=0
            self.Data.to_csv(self.path,index=False)

            if self.count <= self.len_data:
                self.count +=self.count+100
            
            self.takeprocess.setValue(self.count)

        
        
            

app=QApplication(sys.argv)

window=set()
window.setWindowTitle("TUAN ANH")
window.show()
sys.exit(app.exec_())
